// Karma configuration
// Generated on Fri Oct 17 2014 09:21:18 GMT+0200 (CEST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '../',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        'vendors/angular/angular.js',
        'vendors/angular-mocks/angular-mocks.js',
        'vendors/angular-route/angular-route.js',
        'vendors/angular-translate/angular-translate.js',
        'vendors/ng-table/ng-table.js',
        'vendors/angular-bootstrap/ui-bootstrap-tpls.min.js',
        'vendors/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
        'app/app.js',
        'app/config.js',
        'app/modules/**/*.js',
        'test/spec/**/*.js'
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        'app/scripts/**/!(sortable)*.js': 'coverage',
        'app/modules/**/!(app)*.js': 'coverage'
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'coverage'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_ERROR,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    coverageReporter: {
        dir: 'reports/coverage',
        reporters: [
            { type: 'html', subdir: 'html' },
        ]
    }
  });
};
