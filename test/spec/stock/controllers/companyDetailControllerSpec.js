(function () {
    'use strict';

    describe('Controller: companyDetailController', function () {
        beforeEach(module('ngStockApp'));

        var companyDetailController;
        var CompanyProducts;

        beforeEach(inject(function ($controller) {

            CompanyProducts = [
                {
                  "product":"TYRES AUTO",
                  "description":"Neumáticos",
                  "price":120.45
                },
                {
                  "product":"VINIL AUTO",
                  "description":"Abrillantador regenerador de salpicaderos y gomas",
                  "price":20.45
                },
                {
                  "product":"AIR FRESHENER AUTO",
                  "description":"Ambientador",
                  "price":2.45
                }
            ];


            companyDetailController = $controller('companyDetailController', {
                Company: CompanyProducts
            });
        }));

        describe('Initialization ', function () {
            it('should initialize all necesary vars', function () {
                companyDetailController.init();
                expect(companyDetailController.menu).toBeDefined();
                expect(companyDetailController.menu.company).toBeDefined();
                expect(companyDetailController.menu.company.products).toBeDefined();
                expect(companyDetailController.menu.company.products.length).toBe(3);
            });
        });
    });
}());
