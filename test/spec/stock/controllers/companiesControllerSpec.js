(function () {
    'use strict';

    describe('Controller: companiesController', function () {
        beforeEach(module('ngStockApp'));

        var companiesController;
        var Companies;
        var $location;

        beforeEach(inject(function ($controller, _$location_, $filter) {

            $location = _$location_;

            Companies = [
                {
                  "id":"001",
                  "name":"Europcar",
                  "city":"Albacete"
                },
                {
                  "id":"002",
                  "name":"Avis",
                  "city":"Madrid"
                },
                {
                  "id":"003",
                  "name":"Alquiauto",
                  "city":"Barcelona"
                }
            ];


            companiesController = $controller('companiesController', {
                $location: $location,
                $filter: $filter,
                Companies: Companies
            });
        }));

        describe('Initialization ', function () {
            it('should initialize all necesary vars', function () {
                spyOn(companiesController,'initTableParam');
                companiesController.init();
                expect(companiesController.menu).toBeDefined();
                expect(companiesController.menu.filters).toBeDefined();
                expect(companiesController.menu.companies).toBeDefined();
                expect(companiesController.menu.companies.length).toBe(3);
                expect(companiesController.initTableParam).toHaveBeenCalled();
            });
        });

        describe('viewCompanyDetails ', function () {
            it('should redirect', function () {
                spyOn($location,'path');
                companiesController.viewCompanyDetails(1);
                expect($location.path).toHaveBeenCalled();
            });
        });

        describe('initTableParam ', function () {
            it('should initialize table params', function () {
                companiesController.initTableParam();
                expect(companiesController.tableParams).toBeDefined();
            });
        });

        describe('getData ', function () {
            it('should insert data into the tableParams', function () {
                //TODO
            });
        });
    });
}());
