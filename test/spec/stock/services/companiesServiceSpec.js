(function () {
    'use strict';

    describe('Service: CompanyService', function () {
        beforeEach(module('ngStockApp'));

        var CompanyService;
        var apiUrl;
        var $httpBackend;
        var authRequestHandler;

        beforeEach(inject(function (_CompanyService_, _$httpBackend_, _stockConfig_) {
            CompanyService = _CompanyService_;
            $httpBackend = _$httpBackend_;
            apiUrl = _stockConfig_.apiUrl;

        }));

        describe('getCompanies', function () {
          beforeEach( function () {
                authRequestHandler = $httpBackend.whenGET(apiUrl + '/getsamplecompanies?');
            });
            it('Should send a request and get a success response with the companies list', function () {
                authRequestHandler.respond(200,[{"id": "001", "name": "Europcar", "city": "Albacete"}, {"id": "002", "name": "Avis", "city": "Madrid"}, {"id": "003", "name": "Alquiauto", "city": "Barcelona"}]);
                var result = CompanyService.getCompanies();
                $httpBackend.flush();
                expect(result.$$state.value).toBeDefined();
                expect(result.$$state.value.length).toBe(3);
            });
            it('Should send a request and get a error response with the error message', function () {
                authRequestHandler.respond(400,{"messages":["Error obtaining channel list"]});
                var result = CompanyService.getCompanies();
                $httpBackend.flush();
                expect(result.$$state.value).toBeDefined();
                expect(result.$$state.value.length).toBe(0);
            });
        });

        describe('getCompanyById', function () {
          beforeEach( function () {
                authRequestHandler = $httpBackend.whenGET(apiUrl + '/home/api/getsampleproductsbycompany?company=1');
                });
                it('Should send a request and get a success response with the company details', function () {
                    authRequestHandler.respond(200,[{"product": "TYRES AUTO", "description": "Neumáticos", "price": 120.45}, {"product": "VINIL AUTO", "description": "Abrillantador regenerador de salpicaderos y gomas", "price": 20.45}, {"product": "AIR FRESHENER AUTO", "description": "Ambientador", "price": 2.45}]);
                    var result = CompanyService.getCompanyById(1);
                    $httpBackend.flush();
                    expect(result.$$state.value).toBeDefined();
                    expect(result.$$state.value.length).toBe(3);
                });
                it('Should send a request and get a error response with the error message', function () {
                    authRequestHandler.respond(400,{"messages":["Error obtaining channel list"]});
                    var result = CompanyService.getCompanyById(1);
                    $httpBackend.flush();
                    expect(result.$$state.value).toBeDefined();
                    expect(result.$$state.value.length).toBe(0);
                });
            });
        });
}());
