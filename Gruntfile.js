// Generated on 2014-08-04 using generator-angular 0.9.5
'use strict';

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt, {
        pattern: 'grunt-*',
        config: './package.json',
        scope: ['dependencies', 'devDependencies']
    });

    // Configurable paths for the application
    var appConfig = {
        app: require('./bower.json').appPath || 'app',
        dist: 'dist',
        zipFile: 'build.dist.zip'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        root: appConfig,

        //creates index.html from index.tpl.html and includes app js and css files.
        includeSource: {
            options: {
                basePath: 'app',
                baseUrl: '',
                templates: {
                    html: {
                        js: '<script src="{filePath}"></script>',
                        css: '<link rel="stylesheet" type="text/css" href="{filePath}" />'
                    }
                }
            },
            dev: {
                files: {
                    'app/index.html': 'app/index.tpl.html'
                }
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= root.dist %>/{,*/}*',
                        '!<%= root.dist %>/.git*'
                    ]
                }]
            },
            server: '.tmp'
        },

        // Make a zipfile
        compress: {
            main: {
                options: {
                    archive: 'built/<%= root.zipFile %>'
                },
                files: [
                    {src: ['**/*'], expand: true, cwd: 'dist/'}, // includes files in path
                ]
            }
        },

        // Automatically inject Bower components into the app
        wiredep: {
            options: {},
            app: {
                src: ['<%= root.app %>/index.html'],
                ignorePath:  /\.\.\//
            }
        },

        // Renames files for browser caching purposes
        filerev: {
            dist: {
                src: [
                    '<%= root.dist %>/scripts/{,*/}*.js',
                    '<%= root.dist %>/styles/{,*/}*.css',
                    '<%= root.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                    '<%= root.dist %>/styles/fonts/*'
                ]
            }
        },

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            html: '<%= root.app %>/index.html',
            options: {
                dest: '<%= root.dist %>',
                flow: {
                    html: {
                        steps: {
                            js: ['concat', 'uglifyjs'],
                            css: ['cssmin']
                        },
                        post: {}
                    }
                }
            }
        },

        // Performs rewrites based on filerev and the useminPrepare configuration
        usemin: {
            html: ['<%= root.dist %>/{,*/}*.html'],
            css: ['<%= root.dist %>/styles/{,*/}*.css'],
            options: {
                assetsDirs: [
                    '<%= root.dist %>',
                    '<%= root.dist %>/images'
                ]
            }
        },

        // The following *-min tasks will produce minified files in the dist folder
        // By default, your `index.html`'s <!-- Usemin block --> will take care of
        // minification. These next options are pre-configured if you do not wish
        // to use the Usemin blocks.
        cssmin: {
            dist: {
                files: {
                    '<%= root.dist %>/assets/styles/main.css': [
                        '.tmp/styles/{,*/}*.css'
                    ]
                }
            }
        },

        uglify: {
            dist: {
                files: {
                    '<%= root.dist %>/scripts/scripts.js': [
                        '<%= root.dist %>/scripts/scripts.js'
                    ]
                }
            }
        },

        concat: {
            dist: {}
        },

        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= root.app %>/assets/images',
                    src: '{,*/}*.{png,jpg,jpeg,gif}',
                    dest: '<%= root.dist %>/assets/images'
                }]
            }
        },
        // ngAnnotate tries to make the code safe for minification automatically by
        // using the Angular long form for dependency injection. It doesn't work on
        // things like resolve or inject so those have to be done manually.
        ngAnnotate: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/concat/scripts',
                    src: '*.js',
                    dest: '.tmp/concat/scripts'
                }]
            }
        },

        // Replace Google CDN references
        cdnify: {
            dist: {
                html: ['<%= root.dist %>/*.html']
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= root.app %>',
                        dest: '<%= root.dist %>',
                        src: [
                            '*.{ico,png,txt}',
                            '.htaccess',
                            'index.html',
                            'views/{,*/}*.html',
                            'modules/*/views/**',
                            'assets/images/{,*/}*.{webp}',
                            'assets/fonts/*',
                            'i18n/*'
                        ]
                    },
                    {
                        expand: true,
                        cwd: '.tmp/images',
                        dest: '<%= root.dist %>/assets/images',
                        src: ['generated/*']
                    }
                ]
            },
            styles: {
                expand: true,
                cwd: '<%= root.app %>/assets/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            }
        },

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            dist: [
                'copy:styles',
                'imagemin'
            ]
        },

        // Test settings
        karma: {
            unit: {
                configFile: 'test/karma.conf.js'
            }
        },

        //Documentation
        ngdocs: {
            options: {
                dest: 'app/docs',
                title: 'Documentación',
                bestMatch: true
            },
            api: {
                src: ['app/scripts/**/*.js', 'app/modules/**/*.js'],
                title: 'API Documentation'
            }
        },

        // Compiles Sass to CSS and generates necessary files if requested
        compass: {
            dist: {
                options: {
                    sassDir: 'app/assets/styles/scss',
                    cssDir: 'app/assets/styles',
                    noLineComments: true,
                    outputStyle: 'compact'
                }
            },
            server: {
                options: {
                    debugInfo: true
                }
            }
        },

        // output = yml paramteres + template file
        parameters: {
            dist: {
                files: {
                    'app/config.js': ['app/config/config.yml', 'app/config/templates/config.js.tpl']
                }
            }
        },

        symlink: {
            docroot: {
                target: '../vendors',
                link: 'app/vendors',
                options: {
                    overwrite: true,
                    force: true
                }
            }
        }
    });

    grunt.task.registerMultiTask('parameters', 'Parsing parameters files to angularjs constant configuration.', function() {
        this.files.forEach(function(file) {

            var input = grunt.file.readYAML(file.src[0]),
                imports = input.imports ? input.imports : false,
                template = file.src[1] ? grunt.file.read(file.src[1]) : false,
                output = '';

            var text = JSON.stringify(input, function (k, v) {return (k=='imports')?undefined:v;}, 4);
            if (imports !== false) {
                for (var i=imports.length-1; i>=0; --i) {
                    var obj = grunt.file.readJSON(imports[i]);
                    for (var item in obj) {
                        input[item] = obj[item];
                    }
                }
            }
            //console.log(JSON.stringify(input, null, 4));

            grunt.template.addDelimiters('delimiters1', '{{', '}}');
            output = grunt.template.process(text, {
                data: input,
                delimiters: 'delimiters1'
            });

            if (template !== false) {
                output = grunt.template.process(template, {
                    data: {
                        params: output
                    },
                    delimiters: 'delimiters1'
                });
            }

            grunt.file.write(file.dest, output);
            grunt.log.writeln('File "' + file.dest + '" created.');
        });
    });

    grunt.registerTask('test', [
        'clean:server',
        'karma:unit'
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'useminPrepare',
        'concurrent:dist',
        'concat',
        'ngAnnotate',
        'copy:dist',
        'cdnify',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ]);

    grunt.registerTask('doc', [
        'ngdocs'
    ]);

    grunt.registerTask('update', [
        'parameters',
        'compass:dist',
        'includeSource',
        'wiredep',
        'symlink'
    ]);

    grunt.registerTask('default', [
        'compass',
        'test',
        'build'
    ]);
};
