# Instalación

Prerequisitos:

- npm.
- Grunt.
- Bower.

Pasos para compilar el proyecto:

- Instalamos todo lo que el package.json necesita.
```sh
$ npm install
```
- Instalamos los paquetes de terceros necesarios.
```sh
$ bower install
```
- Renombramos el fichero **parameters.json.sample** a **parameters.json** y lo editamos para poner las rutas correctas.
- Lanzamos el siguiente comando para generar el **index.html** con todos los ficheros js y css necesarios.
```sh
$ grunt update
```

Con esto ya tenemos todo preparado para acceder al index.html dentro del directorio app de nuestra aplicación.

# Tasks de Grunt

- update: Lee los ficheros de configuración y crea el index.html.
- test: Lanza la batería de tests unitarios. Este comando genera un directorio llamado coverage con html que nos informa de la cobertura de código en cuanto a tests.
- doc: Genera la documentación en html en el directorio app/docs.
- build: Genera el directorio dist con el proyecto compilado y ofuscado.

# Información adicional

Proyecto realizado siguiendo la guía de estilos de **Todd Motto**.