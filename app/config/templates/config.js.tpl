(function(){
'use strict';

var config = {{=params}};

angular.module('ngConfig', [])
    .constant('mainConfig', config.main)
    .constant('stockConfig', config.stockConfig);
})();
