(function () {
    'use strict';

    /**
     * @author José Carlos Vílchez Torrado
     * @ngdoc overview
     * @name ngMainApp
     * @description
     * # ngMainApp
     *
     * Main Module.
     * This module contains the generic application functionality.
     */
    angular.module('ngMainApp', [
        'ngConfig',
        'ngStockApp',
        'ngRoute',
        'pascalprecht.translate'
    ])

    .config(function ($routeProvider, $httpProvider) {
        $routeProvider
            .when('/', {
                redirectTo: '/companies'
            })
            .otherwise({
                redirectTo: '/'
            });
        $httpProvider.defaults.useXDomain = true;
    })
    .config(['$translateProvider', function ($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: 'i18n/',
            suffix: '.json'
        })
        .registerAvailableLanguageKeys(['es', 'en'])
        .determinePreferredLanguage()
        .fallbackLanguage('en');
    }]);
}());
