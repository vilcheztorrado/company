(function () {
    'use strict';

    /**
     * @author José Carlos Vílchez Torrado
     * @ngdoc controller
     * @name ngStockApp.controllers:companiesController
     * @requires ngStockApp.companiesController
     * @description
     * # companiesController
     * Controller used to manage the companies list.
     */

    function companiesController($filter, $location, Companies, ngTableParams) {

        var vm = this;

        ////////////

        vm.init = init;
        vm.initTableParam = initTableParam;
        vm.getData = getData;
        vm.viewCompanyDetails = viewCompanyDetails;

        ////////////

        function getData ($defer, params) {
                var data = vm.menu.companies || [];
                var filteredData = params.filter() ? $filter('filter')(data, params.filter().allFields) : data;
                var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                data = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                params.total(orderedData.length);
                $defer.resolve(data);
        };

        /**
         * @ngdoc method
         * @name ngStockApp.controllers:companiesController#initTableParam
         * @propertyOf ngStockApp.controllers:companiesController
         * @description
         * Building the companies table.
         */
        function initTableParam () {
            vm.tableParams = new ngTableParams({
                page: 1,
                count: 10,
                filter: vm.menu.filters
            }, {
                total: vm.menu.companies.length,
                getData: vm.getData
            });
        };

        /**
         * @ngdoc method
         * @name ngStockApp.controllers:companiesController#viewCompanyDetails
         * @propertyOf ngStockApp.controllers:companiesController
         * @description
         * Redirect to the company detail URL.
         */
        function viewCompanyDetails (companyId) {
            $location.path("/companies/" + companyId);
        };

        /**
         * @ngdoc method
         * @name ngStockApp.controllers:companiesController#init
         * @methodOf ngStockApp.controllers:companiesController
         * @description
         * Function used to initialize all necessary vars.
         */
        function init () {

            vm.menu = {};
            vm.menu.filters = {
                allFields: ''
            };
            vm.menu.companies = Companies;

            vm.initTableParam();

        }

        vm.init();
    }

    angular
        .module('ngStockApp')
        .controller('companiesController', companiesController);
}());
