(function () {
    'use strict';

    /**
    * @author José Carlos Vílchez Torrado
    * @ngdoc controller
    * @name ngStockApp.controllers:companyDetailController
    * @requires ngStockApp.companyService
    * @description
    * # companyDetailController
    * Controller used to manage the detail company info.
    */
    function companyDetailController(Company) {

        var vm = this;

        ////////////

        vm.init = init;

        ////////////

        /**
         * @ngdoc method
         * @name ngStockApp.controllers:companyDetailController#init
         * @methodOf ngStockApp.controllers:companyDetailController
         * @description
         * Function used to initialize all necessary vars.
         */
        function init () {
            vm.menu = {};
            vm.menu.company = {};
            vm.menu.company.products = Company;
        };
        init();
    }

    angular
        .module('ngStockApp')
        .controller('companyDetailController', companyDetailController);
}());
