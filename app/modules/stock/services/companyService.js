(function () {
    'use strict';

    /**
    * @author José Carlos Vílchez Torrado
     * @ngdoc service
     * @name ngStockApp.CompanyService
     * @requires $http, $log, stockConfig 
     * @description
     *
     * Get the list of available channels.
     */
    function CompanyService($http, $log, stockConfig) {

        //// Available Methods  ////

        return {
            getCompanies: getCompanies,
            getCompanyById: getCompanyById
        };

        /**
         * @ngdoc method
         * @name ngStockApp.CompanyService#getCompanies
         * @methodOf ngStockApp.CompanyService
         * @description
         * Get the company list from back and call internal method for resolve the success and error cases.
         * @returns {Array} result is the company list if all work fine, empty Array if the request fail.
         */
        function getCompanies() {
            return $http.get(stockConfig.apiUrl + stockConfig.companiesUrl)
                .then(getCompaniesComplete)
                .catch(getCompaniesFailed);

            /**
             * @ngdoc method
             * @name ngStockApp.CompanyService#getCompaniesComplete
             * @methodOf ngStockApp.CompanyService
             * @param {Object} response is the response obtained.
             * @description
             * Get the JSON response.
             * @returns {Array} result is the company list Array.
             */
            function getCompaniesComplete(response) {
                return response.data;
            }

            /**
             * @ngdoc method
             * @name ngStockApp.CompanyService#getCompaniesFailed
             * @methodOf ngStockApp.CompanyService
             * @param {Object} error is the error message.
             * @description
             * Get the server error and print it.
             * @returns {Array} result is a empty Array.
             */
            function getCompaniesFailed(error) {
                $log.error('Error XHR failed for getCompanies');
                return [];
            }
        }

        /**
         * @ngdoc method
         * @name ngStockApp.CompanyService#getCompanyById
         * @methodOf ngStockApp.CompanyService
        * @param {int} id is the company id to get.
         * @description
         * Get the company detail info from back and call internal method for resolve the success and error cases.
         * @returns {Array} result is the company detail if all work fine, empty Array if the request fail.
         */
        function getCompanyById(id) {
            return $http.get(stockConfig.apiUrl + stockConfig.companyUrl + "?company=" + id)
                .then(getCompaniesComplete)
                .catch(getCompaniesFailed);

            /**
             * @ngdoc method
             * @name ngStockApp.CompanyService#getCompaniesComplete
             * @methodOf ngStockApp.CompanyService
             * @param {Object} response is the response obtained.
             * @description
             * Get the JSON response.
             * @returns {Array} result is the company detail.
             */
            function getCompaniesComplete(response) {
                return response.data;
            }

            /**
             * @ngdoc method
             * @name ngStockApp.CompanyService#getCompaniesFailed
             * @methodOf ngStockApp.CompanyService
             * @param {Object} error is the error message.
             * @description
             * Get the server error and print it.
             * @returns {Array} result is a empty Array.
             */
            function getCompaniesFailed(error) {
                $log.error('Error XHR failed for getCompany');
                return [];
            }
        }
    }

    angular
        .module('ngStockApp')
        .factory('CompanyService', CompanyService);
}());
