(function () {
    'use strict';

    /**
     * @author José Carlos Vílchez Torrado
     * @ngdoc overview
     * @name ngStockApp
     * @description
     * # ngStockApp
     *
     * Stock Manager Module.
     * This module contains all the functionality to handle routing for stock.
     */
    angular.module('ngStockApp', [
        'ngConfig',
        'ngRoute',
        'ngTable',
        'ui.bootstrap'
    ])
        .config(['$routeProvider', 'stockConfig', function ($routeProvider, stockConfig) {
            $routeProvider
                .when('/companies', {
                    templateUrl: stockConfig.path + 'views/main.html',
                    controller: 'companiesController',
                    controllerAs: 'companiesController',
                    resolve: {
                        Companies: ['CompanyService', function (CompanyService) {
                            return CompanyService.getCompanies()
                                .then(function (response) {
                                    return response;
                                })
                                .catch(function () {
                                    return [];
                                });
                        }]
                    }
                })
                .when('/companies/:companyId', {
                    templateUrl: stockConfig.path + 'views/viewDetails.html',
                    controller: 'companyDetailController',
                    controllerAs: 'companyDetailController',
                    resolve: {
                        Company: ['$route', 'CompanyService', function ($route, CompanyService) {
                            var id = $route.current.params.companyId;
                            return CompanyService.getCompanyById(id)
                                .then(function (response) {
                                    return response;
                                })
                                .catch(function () {
                                    return [];
                                });
                        }]
                    }
                });
        }]);
}());
